const fs = require("fs");
const jsonfile = require('jsonfile');

let funcoes = {
    salvaResultadoArquivo: (nomeArquivo, resultado) => {
        let file = __dirname + `/${nomeArquivo}.json`;
        fs.createWriteStream(file);
        jsonfile.writeFile(file, resultado, { spaces: 2 }, function (err) { });
    },

    criaPaginaHTML: (nomeArquivo, arr) => {
        let file = __dirname + `/${nomeArquivo}.html`;
        
        let style = `
    <style>
        *{box-sizing: border-box;}
        body{
            background: black;
        }
        .info{
            height: 500px;
            width: 100%;
        }
        h3{
            border-left: 4px solid chocolate;
            padding-left: 7px;
            color: white;
        }
        .info .frame{
            height: 89%;
        }
        .info img{
            height: 89%;
            float: left;
        }
        .stats{
            float: left;
            width: 590px;
            height: 100%;
            margin-left: 5px;
            border-left: 4px solid chocolate;
            padding: 40px 0px 7px 8px;
            background: antiquewhite;
        }
    </style>`;
        let content = '';

        arr.map((modelo, i) => {
            entriesModelo = Object.entries(modelo);

            divAttrModelo = '';
            entriesModelo.map(entrie => {
                divAttrModelo += `${entrie[0]}: ${entrie[1]}<br>`;
            });
        
            content += `
    <div class="info">
        <h3>${modelo.nome} (${i+1}/${arr.length})</h3>
        <img src="${modelo.image}">
        <div class="frame">
            <div class="stats">${divAttrModelo}</div>
        </div>
    </div>\n`;
        });
        
        let stream = fs.createWriteStream(file);
        stream.once('open', function (fd) {
            stream.write('<html>');
            stream.write(style);
            stream.write(content);
            stream.write('</html>');
            stream.end();
        });
    },

    kgToLbs: (kg) => {
        return kg * 2.20462;
    },

    mToFtIn: (m) => {
        pes = funcoes.mToFt(m).toString().split('.')[0] + `'`;
        polegadas = (parseFloat('0.' + funcoes.mToFt(m).toString().split('.')[1]) * 12).toFixed(2);
        if (polegadas !== 0)
            polegadas += `"`;

        return pes + polegadas;
    },

    mToFt: (m) => {
        return m * 3.2808398950131;
    },

    filtrarModel: (filtro, arr) => {
        if (arr === undefined)
            arr = funcoes.getAllModels();
            
        let regexModelEach = new RegExp('\\$modelEach', 'g');
        let filtroTest = eval(filtro.replace(regexModelEach, 'arr[0]'));
        
        if (filtroTest !== true && filtroTest !== false) {
            console.log(new Error('Deve passar um filtro que retorne um boolean'));
            return null;
        }

        filtro = filtro.replace(regexModelEach, 'model');
            
        return arr.filter((model, i) => {
            let retorno = eval(filtro);
            return retorno;
        });
    },

    getKeysPesquisaveis: () => {
        let keys = [];

        funcoes.getAllModels().forEach((model, i) => {
            Object.keys(model).forEach(key => {
                keys[key] = key;
            });
        });
        keys = Object.keys(keys);

        return keys;
    },

    getExemploModel: () => {
        let exemplo = {};

        funcoes.getAllModels().forEach((model, i) => {
            Object.keys(model).forEach(key => {
                exemplo[key] = model[key];
            });
        });

        return exemplo;
    },

    getAllModels: () => {
        let contents = fs.readFileSync("data.json");
        let jsonContent = JSON.parse(contents);

        return jsonContent;
    }
}

module.exports = funcoes;