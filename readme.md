# searchScoreland

Módulo que tem dados de modelos(+18) retirado do site scoreland e permite filtrar usando atributos da modelo. 

## Exemplo de modelo com todos os atributos possíveis
```javascript
{
    nome: 'Izabella',
    photos: '369',
    video: '19 mins',
    measurements: '40-27-36',
    height: '5\'4"',
    weight: '135 lbs.',
    braSize: '36C',
    ethnicity: 'White',
    hairColor: 'Brunette',
    bodyType: 'Stacked',
    birthday: 'July 15',
    location: 'Katowice, Poland',
    occupation: 'Interior Decorator',
    website: 'jessicaturner.com',
    camPage: 'Streamate',
    maritalStatus: 'Single'
}
```

## Funções
### getKeysPesquisaveis()

Retorna array que mostra todos os atributos disponíveis para usar como filtro.

### getExemploModel()

Retorna um exemplo de modelo com todos os atributos possíveis.

### getAllModels()

Retorna todas as models.

### filtrarModel(filtro [,arr])

Filtra modelo passando uma String que será uma condição aonde o `$modelEach` será uma variável para poder filtrá-las. Paramêtro `arr` serve para filtrar um array de modelos, caso não use-o o método irá filtrar por todas.

```javascript
const searchScoreland = require('./funcoes');

//Buscar modelos que tenham menos ou igual à 50kg
let modelLte50kg = searchScoreland.filtrarModel(`$modelEach.weight <= '${searchScoreland.kgToLbs(50)} lbs.'`);

//Buscar modelos similares à Jenna Valentine
let similaresJV = searchScoreland.filtrarModel(`$modelEach.braSize !== undefined`); //Antes de usar o indexOf, elimino os valores undefined
    similaresJV = searchScoreland.filtrarModel(`$modelEach.braSize.indexOf('H') >= 0`, similaresJV);
    similaresJV = searchScoreland.filtrarModel(`$modelEach.bodyType == 'Stacked'`, similaresJV);
```

### salvaResultadoArquivo(nomeArquivo, resultado)

Esse método salvará o resultado em um arquivo `.json` podendo passar o caminho, a partir do `__dirname`, juntamente com o nome dele e passando a variável resultado.

```javascript
let modelLte50kg = searchScoreland.filtrarModel(`$modelEach.weight <= '${searchScoreland.kgToLbs(50)} lbs.'`);
//Arquivo a salvar será: __dirname + 'json/modelLte50kg.json'
searchScoreland.salvaResultadoArquivo('json/modelLte50kg', modelLte50kg);
```

### criaPaginaHTML(nomeArquivo, resultado)

Esse método salvará o resultado em um arquivo `.html` podendo passar o caminho, a partir do `__dirname`, juntamente com o nome dele e passando a variável resultado.

```javascript
let nicolePeters = searchScoreland.filtrarModel(`$modelEach.nome == 'Nicole Peters'`); 
searchScoreland.criaPaginaHTML('html/nicolePeters', nicolePeters);
```

### kgToLbs(kg)

Converte kilogramas para libras.
```javascript
let lbs = searchScoreland.kgToLbs(50);
```

### mToFtIn(m)

Converte metros para pés e polegadas.

```javascript
let ftIn = searchScoreland.mToFtIn(2.01);
console.log(ftIn); // 6'7.13"
```

### mToFt(m)

Converte metros para pés.

```javascript
let ft = searchScoreland.mToFtIn(2.01);
console.log(ft); // 6.59448818897633
```

