const fs = require("fs");
const jsonfile = require('jsonfile');

const searchScoreland = require('./funcoes');

let keysPesquisaveis = searchScoreland.getKeysPesquisaveis();
let exemploModel = searchScoreland.getExemploModel();

console.log('Exemplo de modelo com todos os atributos possiveis:\n', exemploModel);
console.log('\n\n');

// Similar à Jenna Valentine
let similaresJV = searchScoreland.filtrarModel(`$modelEach.braSize !== undefined`); //Antes de usar o indexOf, elimino os valores undefined
similaresJV = searchScoreland.filtrarModel(`$modelEach.braSize.indexOf('H') >= 0 || $modelEach.braSize.indexOf('J') >= 0`, similaresJV);
similaresJV = searchScoreland.filtrarModel(`$modelEach.bodyType == 'Stacked'`, similaresJV);

searchScoreland.salvaResultadoArquivo('json/similaresJennaValentine', similaresJV);
console.log('Similar à Jenna Valentine:', similaresJV.length);
console.log('Resultado salvo em JSON.');

searchScoreland.criaPaginaHTML('html/similaresJennaValentine', similaresJV);

// Busca Nicole Peters
/*let nicolePeters = searchScoreland.filtrarModel(`$modelEach.nome == 'Nicole Peters'`);
searchScoreland.criaPaginaHTML('html/nicolePeters', nicolePeters);
*/

// Similar à Nicole Peters
let similaresNP = searchScoreland.filtrarModel(`$modelEach.braSize !== undefined`);
similaresNP = searchScoreland.filtrarModel(`$modelEach.braSize.indexOf('J') >= 0`, similaresNP);
similaresNP = searchScoreland.filtrarModel(`$modelEach.bodyType == 'Voluptuous'`, similaresNP);

searchScoreland.criaPaginaHTML('html/similaresNicolePeters', similaresNP);