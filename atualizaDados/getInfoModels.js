const request = require('request');
const cheerio = require('cheerio');
const jsonfile = require('jsonfile');

let urlInicio = 'http://www.scoreland.com';
let urlPagina1 = 'http://www.scoreland.com/big-boob-models/?page=1';

let models = [];
let nModelsEncontrada = 0;
let terminouBusca = false;

visitaPagina(urlPagina1, 1);

function visitaPagina(url, numeroPagina) {
    request(url, function (error, response, html) {
        if (!error && response.statusCode == 200) {
            var $ = cheerio.load(html);

            if (numeroPagina > 50) {
                // quer dizer que não tem mais pagina
                terminouBusca = true;
            } else {
                console.log('Pagina ' + numeroPagina);

                //Busca modelos
                $('.list_item .box a').each(function (i, element) {
                    let href = $(this).attr('href');
                    nModelsEncontrada++;
                    visitaPaginaModel(urlInicio+href);
                });

                let proxPagina = `http://www.scoreland.com/big-boob-models/?page=${numeroPagina + 1}`;
                visitaPagina(proxPagina, (numeroPagina + 1));
            }
        }
    });
}

// visitaPaginaModel(urlInicio +'/big-boob-models/Jenna-Valentine/6315/?nats=MTAwNC4yLjIuMi41NDUuMC4wLjAuMA');

function visitaPaginaModel(url){
    request(url, function (error, response, html) {
        if (!error && response.statusCode == 200) {
            var $ = cheerio.load(html);

            let jsonModel = {};

            let nomeModel = $('.info.frame .title span').text();
            let image = 'http:' + $('.model_portrait .preview_image img').attr('src');

            jsonModel['nome'] = nomeModel;
            jsonModel['image'] = image;

            $('.info.frame .stat').each(function (i, element) {
                //o indice 0 é o rate, dps será pego, pois deve ser tratado diferente
                if (i>0) {
                    let label = $(this).children('.label').text().trim().replace(/ /g, '');
                    let value = $(this).children('.value').text().trim();
                    
                    label = label.charAt(0).toLowerCase() + label.slice(1);

                    if (label != ''){
                        jsonModel[label.substring(0, label.length-1)] = value;
                    }
                }
            });

            models.push(jsonModel);
            console.log(`Salvou modelo ${models.length} - ${nomeModel}`)

            if (nModelsEncontrada == models.length){
                var file = __dirname + '/../data.json';
                
                jsonfile.writeFile(file, models, { spaces: 2 }, function (err) {
                    console.log(`Gravou json. ${models.length} models.`);
                });
            }
        }
    });
}